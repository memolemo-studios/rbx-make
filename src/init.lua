local function Make(className: string, props: { [string]: any }, children)
	-- functional component
	local _obj = Instance.new(className)

	props = props or {}
	children = children or {}

	-- Setting property tables
	for propName, propValue in pairs(props) do
		_obj[propName] = propValue
	end

	-- Injecting children
	for name, child in pairs(children) do
		child.Parent = _obj
		child.Name = name
	end

	return _obj
end

return Make
