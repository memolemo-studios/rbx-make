/* eslint-disable @typescript-eslint/no-explicit-any */
type InstanceTreeProps<T extends keyof CreatableInstances> = {
	[key in WritablePropertyNames<CreatableInstances[T]>]: CreatableInstances[T][key];
};

// This will prevent from override Instance property as a child
type ReconstructChildren<C extends Instance, T extends Record<string, Instance>> = {
	[K in keyof T]: K extends WritablePropertyNames<C> ? C[K] : T[K];
};

/**
 * Constructs an Instance in `Roact.createElement` like arguments
 * @param className Instance class or functional component
 * @param props
 * @param children
 */
declare function Make<ClassName extends keyof CreatableInstances, Children extends Record<string, Instance> = {}>(
	className: ClassName,
	props?: Partial<InstanceTreeProps<ClassName>>,
	children?: Children,
): CreatableInstances[ClassName] & ReconstructChildren<CreatableInstances[ClassName], Children>;

export = Make;
